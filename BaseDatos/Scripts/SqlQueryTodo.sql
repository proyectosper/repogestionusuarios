
select * from 
(select roles.nombre as Rol,
	   empleados.nombre as Nombre,
       empleados.correo as Correo,
       empleados.fecha_alta, 
       empleados.fecha_actualizacion,
       estados.nombre as Estado, 
       municipios.nombre as Municipio, 
       colonias.nombre as Colonia,
       colonias.codigo_postal as Codigo_Postal,
       direcciones.calle as Calle, 
       direcciones.numero as Numero 
from direcciones join colonias 
on direcciones.colonia_id = colonias.id join estados 
on estados.id = colonias.estado_id join municipios 
on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados 
on empleados.id = direcciones.empleados_id join roles 
on roles.id = empleados.rol_id) as Todo where Nombre like '%ni%' and Estado like '%m%' order by Nombre asc;