-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema control_empleado
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `control_empleado` DEFAULT CHARACTER SET utf8 ;
USE `control_empleado` ;

-- -----------------------------------------------------
-- Table `control_empleado`.`estados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_empleado`.`estados` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `control_empleado`.`colonias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_empleado`.`colonias` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `codigo_postal` INT(11) NOT NULL COMMENT '',
  `nombre` VARCHAR(200) NOT NULL COMMENT '',
  `clave` VARCHAR(45) NOT NULL COMMENT '',
  `estado_id` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_colonias_estados1_idx` (`estado_id` ASC)  COMMENT '',
  CONSTRAINT `fk_colonias_estados1`
    FOREIGN KEY (`estado_id`)
    REFERENCES `control_empleado`.`estados` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 145469
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `control_empleado`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_empleado`.`roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `control_empleado`.`empleados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_empleado`.`empleados` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `nombre` VARCHAR(45) NOT NULL COMMENT '',
  `correo` VARCHAR(45) NOT NULL COMMENT '',
  `rol_id` INT(11) NOT NULL COMMENT '',
  `estado` VARCHAR(45) NOT NULL COMMENT '',
  `fecha_alta` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `fecha_actualizacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `correo_UNIQUE` (`correo` ASC)  COMMENT '',
  INDEX `fk_empleados_roles_idx` (`rol_id` ASC)  COMMENT '',
  CONSTRAINT `fk_empleados_roles`
    FOREIGN KEY (`rol_id`)
    REFERENCES `control_empleado`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `control_empleado`.`direcciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_empleado`.`direcciones` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `calle` VARCHAR(45) NOT NULL COMMENT '',
  `numero` INT(11) NOT NULL COMMENT '',
  `colonia_id` INT(11) NOT NULL COMMENT '',
  `empleados_id` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_direccion_colonia1_idx` (`colonia_id` ASC)  COMMENT '',
  INDEX `fk_direcciones_empleados1_idx` (`empleados_id` ASC)  COMMENT '',
  CONSTRAINT `fk_direccion_colonia1`
    FOREIGN KEY (`colonia_id`)
    REFERENCES `control_empleado`.`colonias` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_direcciones_empleados1`
    FOREIGN KEY (`empleados_id`)
    REFERENCES `control_empleado`.`empleados` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `control_empleado`.`municipios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `control_empleado`.`municipios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `estado_id` INT(11) NOT NULL COMMENT '',
  `clave` INT(11) NOT NULL COMMENT '',
  `nombre` VARCHAR(200) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_municipio_estados1_idx` (`estado_id` ASC)  COMMENT '',
  CONSTRAINT `fk_municipio_estados1`
    FOREIGN KEY (`estado_id`)
    REFERENCES `control_empleado`.`estados` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2493
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

use control_empleado;

