-- Inserccion de una direccion para el registro de un usuario -- 
insert into direcciones values 
(null,'Mutualismo', 2271, 8637); -- requiere el id de colonia en la que vive 
 
-- Consulta que permite conocer el id de la colonia especificando estado, municipio, colonia 
select * from estados join municipios 
on estados.id=municipios.estado_id  join colonias 
on colonias.clave = municipios.clave where estados.nombre like '%coahuila%' 
and municipios.nombre like '%torreón%' 
and colonias.nombre like '%nueva rosita%' order by colonias.nombre asc; 