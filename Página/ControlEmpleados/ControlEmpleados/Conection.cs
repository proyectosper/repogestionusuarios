﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace ControlEmpleados
{
    public class Conection
    {
        MySqlCommand cmd;
        MySqlDataReader read;

        string mensaje;

        public static MySqlConnection Conectar()
        {
            try

            {
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
                builder.Server = "localhost";
                builder.UserID = "root";
                builder.Password = "4321";
                builder.Database = "control_empleado";

                MySqlConnection conn = new MySqlConnection(builder.ToString());
                conn.Open();
                return conn;
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error al conectar", ex);
            }

        }
        public string Insertar(String sentencia_insert)
        {
            try
            {
                cmd = Conectar().CreateCommand();
                cmd.CommandText = sentencia_insert;
                cmd.ExecuteNonQuery();
                mensaje = "Registro Añadido Correctamente";
            }
            catch (Exception)
            {
                mensaje = "Ocurrió un Error al Intentar Registrar";
                throw;
            }
            return mensaje;
        }
        public MySqlDataReader Consultar(String sentencia_select)
        {
            try
            {
                cmd = Conectar().CreateCommand();
                cmd.CommandText = sentencia_select;
                read = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                mensaje = "Ocurrió un Error al Intentar Consultar";
                throw;
            }
            
            return read;
        }
        public String Eliminar(string sentencia_delete)
        {
            try
            {
                cmd = Conectar().CreateCommand();
                cmd.CommandText = sentencia_delete;
                cmd.ExecuteNonQuery();
                mensaje = "Registro Eliminado Corrrectamente";
            }
            catch (Exception)
            {
                mensaje = "Ocurrió un Error al Intentar Eliminar";
                throw;
            }
            return mensaje;
        }
        public String Actualizar(string sentencia_delete)
        {
            try
            {
                cmd = Conectar().CreateCommand();
                cmd.CommandText = sentencia_delete;
                cmd.ExecuteNonQuery();
                mensaje = "Registro Actualizado Corrrectamente";
            }
            catch (Exception)
            {
                mensaje = "Ocurrió un Error al Intentar Actualizar";
                throw;
            }
            return mensaje;
        }
    }
}