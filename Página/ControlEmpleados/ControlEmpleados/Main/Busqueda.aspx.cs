﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;

namespace ControlEmpleados.Main
{
    public partial class Busqueda : System.Web.UI.Page
    {
        Conection con = new Conection();
        MySqlCommand consulta;
        MySqlDataReader read;
        MySqlDataAdapter adapter;
        DataTable dtable;
        GridViewRow row_grid;
        Empleado emp = new Empleado();
        int row;
        int resultados = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Cargar_GridDatos();

            if (!IsPostBack)
            {
                Cargar_GridDatos();
                string sentencia = "SELECT id, nombre FROM estados;";
                Base.Cargar_DropEstatico(sentencia, DropEstados, "-- Estados --");
                DropMunicipios.SelectedIndex = 0;
                DropColonias.SelectedIndex = 0;
            }
        }



        protected void BtnFiltrar_Click(object sender, EventArgs e)
        {
            if (DropEstados.SelectedIndex != 0)
            {
                if (TbxNombre.Text.Trim() != String.Empty)
                {
                    if (DropMunicipios.SelectedIndex != 0)
                    {
                        if (DropColonias.SelectedIndex != 0)
                        {
                            consulta = new MySqlCommand(string.Format("select * from (select roles.nombre as Rol," +
                                                                        "empleados.nombre as Nombre," +
                                                                        "empleados.correo as Correo," +
                                                                        "empleados.fecha_alta," +
                                                                        "empleados.fecha_actualizacion," +
                                                                        "estados.nombre as Estado," +
                                                                        "municipios.nombre as Municipio," +
                                                                        "colonias.nombre as Colonia," +
                                                                        "colonias.codigo_postal as Codigo_Postal," +
                                                                        "direcciones.calle as Calle," +
                                                                        "direcciones.numero as Numero " +
                                                                 "from direcciones join colonias " +
                                                                 "on direcciones.colonia_id = colonias.id join estados " +
                                                                 "on estados.id = colonias.estado_id join municipios " +
                                                                 "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                                                 "on empleados.id = direcciones.empleados_id join roles on roles.id = empleados.rol_id) as Todo " +
                                                                 "where Nombre like '%" + TbxNombre.Text + "%'and Estado like '%" + DropEstados.SelectedItem + "%'" +
                                                                 " and Municipio like '%" + DropMunicipios.SelectedItem + "%' and Colonia like '%" + DropColonias.SelectedItem + "%' order by Nombre asc; "), Conection.Conectar());
                            adapter = new MySqlDataAdapter(consulta);
                            dtable = new DataTable();
                            resultados = adapter.Fill(dtable);
                            LblResultados.Text = resultados.ToString();
                            GvEmpleados.DataSource = dtable;
                            GvEmpleados.DataBind();
                            Conection.Conectar().Close();
                            return;

                        }
                        consulta = new MySqlCommand(string.Format("select * from (select roles.nombre as Rol," +
                                                                        "empleados.nombre as Nombre," +
                                                                        "empleados.correo as Correo," +
                                                                        "empleados.fecha_alta," +
                                                                        "empleados.fecha_actualizacion," +
                                                                        "estados.nombre as Estado," +
                                                                        "municipios.nombre as Municipio," +
                                                                        "colonias.nombre as Colonia," +
                                                                        "colonias.codigo_postal as Codigo_Postal," +
                                                                        "direcciones.calle as Calle," +
                                                                        "direcciones.numero as Numero " +
                                                                 "from direcciones join colonias " +
                                                                 "on direcciones.colonia_id = colonias.id join estados " +
                                                                 "on estados.id = colonias.estado_id join municipios " +
                                                                 "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                                                 "on empleados.id = direcciones.empleados_id join roles on roles.id = empleados.rol_id) as Todo " +
                                                                 "where Nombre like '%" + TbxNombre.Text + "%'and Estado like '%" + DropEstados.SelectedItem + "%'" +
                                                                 " and Municipio like '%" + DropMunicipios.SelectedItem + "%'  order by Nombre asc; "), Conection.Conectar());
                        adapter = new MySqlDataAdapter(consulta);
                        dtable = new DataTable();
                        resultados = adapter.Fill(dtable);
                        LblResultados.Text = resultados.ToString();
                        GvEmpleados.DataSource = dtable;
                        GvEmpleados.DataBind();
                        Conection.Conectar().Close();
                        return;
                    }
                    //Selecciona Todos los Datos con el filtro de Nombre y Estado
                    consulta = new MySqlCommand(string.Format("select * from (select roles.nombre as Rol," +
                                                                        "empleados.nombre as Nombre," +
                                                                        "empleados.correo as Correo," +
                                                                        "empleados.fecha_alta," +
                                                                        "empleados.fecha_actualizacion," +
                                                                        "estados.nombre as Estado," +
                                                                        "municipios.nombre as Municipio," +
                                                                        "colonias.nombre as Colonia," +
                                                                        "colonias.codigo_postal as Codigo_Postal," +
                                                                        "direcciones.calle as Calle," +
                                                                        "direcciones.numero as Numero " +
                                                                 "from direcciones join colonias " +
                                                                 "on direcciones.colonia_id = colonias.id join estados " +
                                                                 "on estados.id = colonias.estado_id join municipios " +
                                                                 "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                                                 "on empleados.id = direcciones.empleados_id join roles on roles.id = empleados.rol_id) as Todo " +
                                                                 "where Nombre like '%" + TbxNombre.Text + "%'and Estado like '%" + DropEstados.SelectedItem + "%' order by Nombre asc; "), Conection.Conectar());
                    adapter = new MySqlDataAdapter(consulta);
                    dtable = new DataTable();
                    resultados = adapter.Fill(dtable);
                    LblResultados.Text = resultados.ToString();
                    GvEmpleados.DataSource = dtable;
                    GvEmpleados.DataBind();
                    Conection.Conectar().Close();
                    return;
                }
                else
                {
                    if (DropMunicipios.SelectedIndex != 0)
                    {
                        if (DropColonias.SelectedIndex != 0)
                        {
                            consulta = new MySqlCommand(string.Format("select * from (select roles.nombre as Rol," +
                                                                        "empleados.nombre as Nombre," +
                                                                        "empleados.correo as Correo," +
                                                                        "empleados.fecha_alta," +
                                                                        "empleados.fecha_actualizacion," +
                                                                        "estados.nombre as Estado," +
                                                                        "municipios.nombre as Municipio," +
                                                                        "colonias.nombre as Colonia," +
                                                                        "colonias.codigo_postal as Codigo_Postal," +
                                                                        "direcciones.calle as Calle," +
                                                                        "direcciones.numero as Numero " +
                                                                 "from direcciones join colonias " +
                                                                 "on direcciones.colonia_id = colonias.id join estados " +
                                                                 "on estados.id = colonias.estado_id join municipios " +
                                                                 "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                                                 "on empleados.id = direcciones.empleados_id join roles on roles.id = empleados.rol_id) as Todo " +
                                                                 "where Estado like '%" + DropEstados.SelectedItem + "%'" +
                                                                 " and Municipio like '%" + DropMunicipios.SelectedItem + "%' and Colonia like '%" + DropColonias.SelectedItem + "%' order by Nombre asc; "), Conection.Conectar());
                            adapter = new MySqlDataAdapter(consulta);
                            dtable = new DataTable();
                            resultados = adapter.Fill(dtable);
                            LblResultados.Text = resultados.ToString();
                            GvEmpleados.DataSource = dtable;
                            GvEmpleados.DataBind();
                            Conection.Conectar().Close();
                            return;
                        }
                        consulta = new MySqlCommand(string.Format("select * from (select roles.nombre as Rol," +
                                                                        "empleados.nombre as Nombre," +
                                                                        "empleados.correo as Correo," +
                                                                        "empleados.fecha_alta," +
                                                                        "empleados.fecha_actualizacion," +
                                                                        "estados.nombre as Estado," +
                                                                        "municipios.nombre as Municipio," +
                                                                        "colonias.nombre as Colonia," +
                                                                        "colonias.codigo_postal as Codigo_Postal," +
                                                                        "direcciones.calle as Calle," +
                                                                        "direcciones.numero as Numero " +
                                                                 "from direcciones join colonias " +
                                                                 "on direcciones.colonia_id = colonias.id join estados " +
                                                                 "on estados.id = colonias.estado_id join municipios " +
                                                                 "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                                                 "on empleados.id = direcciones.empleados_id join roles on roles.id = empleados.rol_id) as Todo " +
                                                                 "where Estado like '%" + DropEstados.SelectedItem + "%'" +
                                                                 " and Municipio like '%" + DropMunicipios.SelectedItem + "%'  order by Nombre asc; "), Conection.Conectar());
                        adapter = new MySqlDataAdapter(consulta);
                        dtable = new DataTable();
                        resultados = adapter.Fill(dtable);
                        LblResultados.Text = resultados.ToString();
                        GvEmpleados.DataSource = dtable;
                        GvEmpleados.DataBind();
                        Conection.Conectar().Close();
                        return;
                    }
                    consulta = new MySqlCommand(string.Format("select * from (select roles.nombre as Rol," +
                                                                       "empleados.nombre as Nombre," +
                                                                       "empleados.correo as Correo," +
                                                                       "empleados.fecha_alta," +
                                                                       "empleados.fecha_actualizacion," +
                                                                       "estados.nombre as Estado," +
                                                                       "municipios.nombre as Municipio," +
                                                                       "colonias.nombre as Colonia," +
                                                                       "colonias.codigo_postal as Codigo_Postal," +
                                                                       "direcciones.calle as Calle," +
                                                                       "direcciones.numero as Numero " +
                                                                "from direcciones join colonias " +
                                                                "on direcciones.colonia_id = colonias.id join estados " +
                                                                "on estados.id = colonias.estado_id join municipios " +
                                                                "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                                                "on empleados.id = direcciones.empleados_id join roles on roles.id = empleados.rol_id) as Todo " +
                                                                "where Estado like '%" + DropEstados.SelectedItem + "%' order by Nombre asc; "), Conection.Conectar());
                    adapter = new MySqlDataAdapter(consulta);
                    dtable = new DataTable();
                    resultados = adapter.Fill(dtable);
                    LblResultados.Text = resultados.ToString();
                    GvEmpleados.DataSource = dtable;
                    GvEmpleados.DataBind();
                    Conection.Conectar().Close();
                    return;
                }
            }
            else
            {
                //Selecciona Todos los Datos con el Filtro de Nombre
                consulta = new MySqlCommand(string.Format("select * from (select roles.nombre as Rol," +
                                                                        "empleados.nombre as Nombre," +
                                                                        "empleados.correo as Correo," +
                                                                        "empleados.fecha_alta," +
                                                                        "empleados.fecha_actualizacion," +
                                                                        "estados.nombre as Estado," +
                                                                        "municipios.nombre as Municipio," +
                                                                        "colonias.nombre as Colonia," +
                                                                        "colonias.codigo_postal as Codigo_Postal," +
                                                                        "direcciones.calle as Calle," +
                                                                        "direcciones.numero as Numero " +
                                                                 "from direcciones join colonias " +
                                                                 "on direcciones.colonia_id = colonias.id join estados " +
                                                                 "on estados.id = colonias.estado_id join municipios " +
                                                                 "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                                                 "on empleados.id = direcciones.empleados_id join roles on roles.id = empleados.rol_id) as Todo " +
                                                                 "where Nombre like '%" + TbxNombre.Text + "%' order by Nombre asc; "), Conection.Conectar());
                adapter = new MySqlDataAdapter(consulta);
                dtable = new DataTable();
                resultados = adapter.Fill(dtable);
                LblResultados.Text = resultados.ToString();
                GvEmpleados.DataSource = dtable;
                GvEmpleados.DataBind();
                Conection.Conectar().Close();
                return;
            }
        }

        private void Cargar_GridDatos()
        {
            consulta = new MySqlCommand(string.Format("select roles.nombre as Rol,empleados.nombre as Nombre,empleados.correo as Correo," +
                                                                   "empleados.fecha_alta, empleados.fecha_actualizacion," +
                                                                   "estados.nombre as Estado, municipios.nombre as Municipio, colonias.nombre as Colonia," +
                                                                   "colonias.codigo_postal as Codigo_Postal, direcciones.calle as Calle, direcciones.numero as Numero " +
                                                                   "from direcciones join colonias " +
                                                                   "on direcciones.colonia_id = colonias.id join estados " +
                                                                   "on estados.id = colonias.estado_id join municipios " +
                                                                   "on municipios.estado_id = estados.id " +
                                                                   "and municipios.clave = colonias.clave join empleados " +
                                                                   "on empleados.id = direcciones.empleados_id join roles " +
                                                                   "on roles.id = empleados.rol_id; "), Conection.Conectar());
            MySqlDataAdapter adapter = new MySqlDataAdapter(consulta);
            DataTable dtable = new DataTable();
            resultados = adapter.Fill(dtable);
            LblResultados.Text = resultados.ToString();
            GvEmpleados.DataSource = dtable;
            GvEmpleados.DataBind();
            Conection.Conectar().Close();

            // Limpiar_Controles();
        }

        protected void DropEstados_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropEstados.SelectedIndex != 0)
            {
                int estado_id = Convert.ToInt32(DropEstados.SelectedValue);
                string sentencia = "SELECT municipios.id, municipios.nombre FROM estados " +
                                     "JOIN municipios ON estados.id = municipios.estado_id WHERE estados.id = " + estado_id + " ORDER BY municipios.nombre ASC;";
                Base.Cargar_DropDinamico(sentencia, DropMunicipios, "-- Municipios --");
            }
        }

        protected void DropMunicipios_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropMunicipios.SelectedIndex != 0)
            {
                int municipio_id = Convert.ToInt32(DropMunicipios.SelectedValue);
                string sentencia = "SELECT colonias.id, colonias.nombre FROM municipios " +
                                     "JOIN colonias ON colonias.estado_id = municipios.estado_id " +
                                     "WHERE municipios.clave = colonias.clave AND municipios.id = " + municipio_id +
                                     " ORDER BY colonias.nombre;";
                Base.Cargar_DropDinamico(sentencia, DropColonias, "-- Colonias --");
            }

        }

        protected void BtnQuitar_Click(object sender, EventArgs e)
        {
            TbxNombre.Text = string.Empty;
            DropEstados.SelectedIndex = 0;
            DropMunicipios.Items.Clear();
            DropColonias.Items.Clear();
            DropMunicipios.Items.Insert(0, "-- Municipios --");
            DropColonias.Items.Insert(0, "-- Colonias --");
        }

        protected void GvEmpleados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
            {
                //  Cuando el Mouse este dentro de el row coloca el estilo
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;" +
                                                                            "this.style.backgroundColor='#ccc'");

                // Cuando el Mouse salga de el row vuelve al estilo normal
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
            Label1.Text = GvEmpleados.SelectedIndex.ToString();
        }              
        protected void Button1_Click(object sender, EventArgs e)
        {
            LblResultados.Text = TextBox1.Text;
        }

        protected void BtnEditar_Click(object sender, EventArgs e)
        {
            GridViewRow gvRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvRow.RowIndex;
            Base.dato_unico = gvRow.Cells[4].Text;

            //LblResultados.Text = index + "  =  " + Base.dato_unico+" Editar";
            
            Response.Redirect("Gestion.aspx");
        }

        protected void BtnEliminar_Click(object sender, EventArgs e)
        {
            GridViewRow gvRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvRow.RowIndex;
            Base.dato_unico = gvRow.Cells[4].Text;

            Lblrespuesta.Text = con.Eliminar(emp.Eliminar(emp.GetId(Base.dato_unico)));
            Cargar_GridDatos();
           // LblResultados.Text = index + "  =  " + Base.dato_unico + " Eliminar";
        }

    }
}