﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace ControlEmpleados.Main
{
    public partial class Gestion_ : System.Web.UI.Page
    {
        Conection con = new Conection();
        MySqlDataReader read;
        int count = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sentencia = "SELECT id, nombre FROM estados;";
                Base.Cargar_DropEstatico(sentencia, DropEstado,"-- Estados --");
                //DropMunicipio.SelectedItem.Value; como sacarle el value o el text

                string abraham = "select * from roles;";
                Base.Cargar_DropEstatico(abraham, DropRol,"-- Roles --");
            }

            //LblAlert.Text = DateTime.Now.ToString();
        }
       
        protected void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                Empleado emp = new Empleado();
                emp.nombre = TbxNombre.Text;
                emp.correo = TbxCorreo.Text;
                emp.estado = 1;

                read = con.Consultar("Select id from roles where nombre = '" + DropRol.SelectedItem + "';");

                count = 0;

                if (read.HasRows)
                {
                    while (read.Read())
                    {
                        emp.rol_id = read.GetInt32(0);
                    }
                    count++;
                }

                con.Insertar(emp.Insertar());

                Direccion dir = new Direccion();
                dir.calle = TbxCalle.Text;
                dir.numero = int.Parse(TbxNumero.Text);
                read = con.Consultar("Select id from empleados where correo = '" + TbxCorreo.Text + "';");

                count = 0;

                if (read.HasRows)
                {
                    while (read.Read())
                    {
                        dir.empleado_id = read.GetInt32(0);
                    }
                    count++;
                }
            
                read = con.Consultar("select colonias.id from estados join municipios " +
                                     "on estados.id = municipios.estado_id  join colonias " +
                                     "on colonias.clave = municipios.clave where estados.nombre like '%"+DropEstado.SelectedItem+"%' " +
                                     "and municipios.nombre like '%"+DropMunicipio.SelectedItem + "%' " +
                                     "and colonias.nombre like '%"+DropColonia.SelectedItem + "%'; ");

                count = 0;

                if (read.HasRows)
                {
                    while (read.Read())
                    {
                        dir.colonia_id = read.GetInt32(0);
                    }
                    count++;
                }

                LblAlert.Text = dir.Insertar();
            }
            catch (Exception)
            {
                LblAlert.Text = "No Puede Dejar Campos Vacios";
            }
            Limpiar_Controles();
        }

        //Metodo que acciona cuando el index cambia al seleccionar una opcion de estados
        protected void DropEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropMunicipio.Items.Clear();
            DropColonia.Items.Clear();
            DropMunicipio.Items.Insert(0, "-- Municipios --");
            DropColonia.Items.Insert(0, "-- Colonias --");
            if (DropEstado.SelectedIndex != 0)
            {
                int estado_id = Convert.ToInt32(DropEstado.SelectedValue);
                string sentencia = "SELECT municipios.id, municipios.nombre FROM estados " +
                                     "JOIN municipios ON estados.id = municipios.estado_id WHERE estados.id = " + estado_id + " ORDER BY municipios.nombre ASC;";
                Base.Cargar_DropDinamico(sentencia, DropMunicipio, "-- Municipios --");
            }
        }
        //Metodo que acciona cuando el index cambia al seleccionar una opcion de municipios
        protected void DropMunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropColonia.Items.Clear();
            DropColonia.Items.Insert(0, "-- Colonias --");
            if (DropMunicipio.SelectedIndex != 0)
            {
                int municipio_id = Convert.ToInt32(DropMunicipio.SelectedValue);
                string sentencia = "SELECT colonias.id, colonias.nombre FROM municipios " +
                                     "JOIN colonias ON colonias.estado_id = municipios.estado_id " +
                                     "WHERE municipios.clave = colonias.clave AND municipios.id = " + municipio_id +
                                     " ORDER BY colonias.nombre;";
                Base.Cargar_DropDinamico(sentencia, DropColonia, "-- Colonias --");
            }
        }
        #region Sin_Automatizar
        ///Metodo Para Cargar el Drop Municipios
        //private void Cargar_DropMunicipios(int estado_id)
        //{
        //    read = con.Consultar("SELECT municipios.id, municipios.nombre FROM estados " +
        //                         "JOIN municipios ON estados.id = municipios.estado_id WHERE estados.id = " + estado_id + ";");

        //    DropMunicipio.DataSource = read;
        //    DropMunicipio.DataTextField = "nombre";
        //    DropMunicipio.DataValueField = "id";
        //    DropMunicipio.DataBind();

        //    int count = 0;


        //    if (read.HasRows)
        //    {
        //        while (read.Read())
        //        {
        //            DropMunicipio.DataValueField = read.GetInt32(0).ToString();
        //            DropMunicipio.DataTextField = read.GetString(1);
        //        }
        //        count++;
        //    }
        //    else
        //    {
        //        LblAlert.Text = "Error en la consulta";
        //    }
        //    read.Close();
        //}
        /////Metodo Para Cargar el Drop Colonias
        //private void Cargar_DropColonias(int municipio_id)
        //{
        //    read = con.Consultar("SELECT colonias.id, colonias.nombre FROM municipios " +
        //                         "JOIN colonias ON colonias.estado_id = municipios.estado_id " +
        //                         "WHERE municipios.clave = colonias.clave AND municipios.id = "+ municipio_id + 
        //                         " ORDER BY colonias.nombre;");

        //    DropColonia.DataSource = read;
        //    DropColonia.DataTextField = "nombre";
        //    DropColonia.DataValueField = "id";
        //    DropColonia.DataBind();

        //    int count = 0;


        //    if (read.HasRows)
        //    {
        //        while (read.Read())
        //        {
        //            DropColonia.DataValueField = read.GetInt32(0).ToString();
        //            DropColonia.DataTextField = read.GetString(1);
        //        }
        //        count++;
        //    }
        //    else
        //    {
        //        LblAlert.Text = "Error en la consulta";
        //    }
        //    read.Close();
        //}
        #endregion

        public void Limpiar_Controles()
        {
            
            Control[] array_Controles = new Control[]
            {
                DropColonia,
                DropMunicipio,
                TbxCalle,
                TbxNombre,
                TbxNumero,
                TbxCorreo
             };
            DropRol.SelectedIndex = 0;
            DropEstado.SelectedIndex = 0;

            foreach (Control item in array_Controles)
            {
                if (item is TextBox)
                {
                    TextBox t = item as TextBox;
                    t.Text = string.Empty;
                }
                else if (item is DropDownList)
                {
                    DropDownList d = item as DropDownList;
                        d.Items.Clear();
                    }
                }
                DropMunicipio.Items.Insert(0, "-- Municipios --");
                DropColonia.Items.Insert(0, "-- Colonias --");
                DropMunicipio.SelectedIndex = 0;
                DropColonia.SelectedIndex = 0;

            } 
        }
    }
