﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace ControlEmpleados.Main
{
    public partial class Gestion : System.Web.UI.Page
    {
        Empleado emp = new Empleado();
        Conection con = new Conection();
        MySqlDataReader read;
        string sentencia,sentencia1,sentencia2;

        protected void Page_Load(object sender, EventArgs e)
        {
            Cargar_GridDatos();

            sentencia1 = "SELECT id, nombre FROM estados;";
            sentencia2 = "SELECT id, nombre FROM roles;";
            if (!IsPostBack)
            {
                Base.Cargar_DropEstatico(sentencia1, DropEstado, "-- Estados --");
                Base.Cargar_DropEstatico(sentencia2, DropRol, "-- Roles --");
                DropEstado.SelectedIndex = 0;
                DropColonia.SelectedIndex = 0;
            }

            if (Base.dato_unico!=string.Empty)
            {
                sentencia = "select empleados.id as Id," +
                            "roles.id as Rol," +
                            "empleados.nombre as Nombre," +
                            "empleados.correo as Correo," +
                            "estados.id as Estado," +
                            "municipios.id as Municipio," +
                            "colonias.id as Colonia," +
                            "direcciones.calle as Calle," +
                            "direcciones.numero as Numero " +
                          "from direcciones join colonias " +
                                "on direcciones.colonia_id = colonias.id join estados " +
                                "on estados.id = colonias.estado_id join municipios " +
                                "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                "on empleados.id = direcciones.empleados_id join roles " +
                                "on roles.id = empleados.rol_id where Correo = '" + Base.dato_unico + "';";
                string sentencia5;
                read = con.Consultar(sentencia);

                if (read.HasRows)
                {
                    while (read.Read())
                    {
                        TbxNombre.Text = read.GetString(2);
                        TbxCorreo.Text = read.GetString(3);
                        Base.Cargar_DropEstatico(sentencia2, DropRol,"-- Roles --");
                        DropRol.SelectedIndex = read.GetInt32(1);
                        Base.Cargar_DropEstatico(sentencia1, DropEstado, "-- Estados --");
                        DropEstado.SelectedIndex = read.GetInt32(4);
                        sentencia5 = "SELECT municipios.id, municipios.nombre FROM estados " +
                                                "JOIN municipios ON estados.id = municipios.estado_id WHERE estados.id = " + read.GetInt32(4) + " ORDER BY municipios.nombre ASC;";
                        Base.Cargar_DropDinamico(sentencia5, DropMunicipio, "-- Municipios --");
                        DropMunicipio.SelectedValue = read.GetString(5);

                        string sentencia = "SELECT colonias.id, colonias.nombre FROM municipios " +
                                    "JOIN colonias ON colonias.estado_id = municipios.estado_id " +
                                    "WHERE municipios.clave = colonias.clave AND municipios.id = " + read.GetInt32(5) +
                                    " ORDER BY colonias.nombre;";
                        Base.Cargar_DropDinamico(sentencia, DropColonia, "-- Colonias --");

                        DropColonia.SelectedValue = read.GetString(6);                      
                        TbxCalle.Text = read.GetString(7);
                        TbxNumero.Text = read.GetString(8);
                        //LblAlert.Text = read.GetString(1) + " " + read.GetString(4) + " " + read.GetString(5) + " " + read.GetString(6);

                        //TbxNombre.ReadOnly = false;
                        //TbxCorreo.ReadOnly = false;
                        //TbxCalle.ReadOnly = false;
                        //TbxNumero.ReadOnly = false;
                    }
                    sentencia5 = "SELECT municipios.id, municipios.nombre FROM estados " +
                                                "JOIN municipios ON estados.id = municipios.estado_id WHERE estados.id = " + read.GetInt32(4) + " ORDER BY municipios.nombre ASC;";
                    Base.Cargar_DropDinamico(sentencia5, DropMunicipio, "-- Municipios --");
                    //DropMunicipio.SelectedIndex = read.GetInt32(5);

                }
                read.Close();

                //LblAlert.Text = "Correo = " + Base.dato_unico;
            }
            else
            { 
            //    TbxNombre.ReadOnly = true;
            //    TbxCorreo.ReadOnly = true;
            //    TbxCalle.ReadOnly = true;
            //    TbxNumero.ReadOnly = true;
               
                
                //DropEstado.ReadOnly = true;
                //DropMunicipio.ReadOnly = true;
                //DropColonia.ReadOnly = true;

                //LblAlert.Text = "Correo Vacio";
            }
            Base.dato_unico = string.Empty;
        }

        protected void DropEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropMunicipio.Items.Clear();
            DropColonia.Items.Clear();
            DropMunicipio.Items.Insert(0, "-- Municipios --");
            DropColonia.Items.Insert(0, "-- Colonias --");
            if (DropEstado.SelectedIndex != 0)
            {
                int estado_id = Convert.ToInt32(DropEstado.SelectedValue);
                string sentencia = "SELECT municipios.id, municipios.nombre FROM estados " +
                                     "JOIN municipios ON estados.id = municipios.estado_id WHERE estados.id = " + estado_id + " ORDER BY municipios.nombre ASC;";
                Base.Cargar_DropDinamico(sentencia, DropMunicipio, "-- Municipios --");
            }
        }

        protected void DropMunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropColonia.Items.Clear();
            DropColonia.Items.Insert(0, "-- Colonias --");
            if (DropMunicipio.SelectedIndex != 0)
            {
                int municipio_id = Convert.ToInt32(DropMunicipio.SelectedValue);
                string sentencia = "SELECT colonias.id, colonias.nombre FROM municipios " +
                                     "JOIN colonias ON colonias.estado_id = municipios.estado_id " +
                                     "WHERE municipios.clave = colonias.clave AND municipios.id = " + municipio_id +
                                     " ORDER BY colonias.nombre;";
                Base.Cargar_DropDinamico(sentencia, DropColonia, "-- Colonias --");
            }
        }

        protected void BtnActualizar_Click(object sender, EventArgs e)
        {

            GridViewRow gvRow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvRow.RowIndex;
            Base.dato_unico = gvRow.Cells[3].Text;


            if (Base.dato_unico != string.Empty)
            {
                sentencia = "select empleados.id as Id," +
                            "roles.id as Rol," +
                            "empleados.nombre as Nombre," +
                            "empleados.correo as Correo," +
                            "estados.id as Estado," +
                            "municipios.id as Municipio," +
                            "colonias.id as Colonia," +
                            "direcciones.calle as Calle," +
                            "direcciones.numero as Numero " +
                          "from direcciones join colonias " +
                                "on direcciones.colonia_id = colonias.id join estados " +
                                "on estados.id = colonias.estado_id join municipios " +
                                "on municipios.estado_id = estados.id and municipios.clave = colonias.clave join empleados " +
                                "on empleados.id = direcciones.empleados_id join roles " +
                                "on roles.id = empleados.rol_id where Correo = '" + Base.dato_unico + "';";
                string sentencia5;
                read = con.Consultar(sentencia);

                if (read.HasRows)
                {
                    while (read.Read())
                    {
                        TbxNombre.Text = read.GetString(2);
                        TbxCorreo.Text = read.GetString(3);
                        Base.Cargar_DropEstatico(sentencia2, DropRol, "-- Roles --");
                        DropRol.SelectedIndex = read.GetInt32(1);
                        Base.Cargar_DropEstatico(sentencia1, DropEstado, "-- Estados --");
                        DropEstado.SelectedIndex = read.GetInt32(4);

                        sentencia5 = "SELECT municipios.id, municipios.nombre FROM estados " +
                                                "JOIN municipios ON estados.id = municipios.estado_id WHERE estados.id = " + read.GetInt32(4) + " ORDER BY municipios.nombre ASC;";
                        Base.Cargar_DropDinamico(sentencia5, DropMunicipio, "-- Municipios --");
                        DropMunicipio.SelectedValue = read.GetString(5);

                        string sentencia = "SELECT colonias.id, colonias.nombre FROM municipios " +
                                    "JOIN colonias ON colonias.estado_id = municipios.estado_id " +
                                    "WHERE municipios.clave = colonias.clave AND municipios.id = " + read.GetInt32(5) +
                                    " ORDER BY colonias.nombre;";
                        Base.Cargar_DropDinamico(sentencia, DropColonia, "-- Colonias --");

                        DropColonia.SelectedValue = read.GetString(6);

                        TbxCalle.Text = read.GetString(7);
                        TbxNumero.Text = read.GetString(8);
                        //LblAlert.Text = read.GetString(1) + " " + read.GetString(2) + " " + read.GetString(3) + " " + read.GetString(4) + " " + read.GetString(5) + " " + read.GetString(6) +" "+ read.GetString(7) + " " + read.GetString(8);

                        TbxNombre.ReadOnly = false;
                        TbxCorreo.ReadOnly = false;
                        TbxCalle.ReadOnly = false;
                        TbxNumero.ReadOnly = false;
                    }
                    sentencia5 = "SELECT municipios.id, municipios.nombre FROM estados " +
                                                "JOIN municipios ON estados.id = municipios.estado_id WHERE estados.id = " + read.GetInt32(4) + " ORDER BY municipios.nombre ASC;";
                    Base.Cargar_DropDinamico(sentencia5, DropMunicipio, "-- Municipios --");
                    //DropMunicipio.SelectedIndex = read.GetInt32(5);
                    Cargar_GridDatos();
                }
                read.Close();
                Cargar_GridDatos();
                //LblAlert.Text = "Correo = " + Base.dato_unico;
            }
            else
            {
                TbxNombre.ReadOnly = true;
                TbxCorreo.ReadOnly = true;
                TbxCalle.ReadOnly = true;
                TbxNumero.ReadOnly = true;

                Cargar_GridDatos();
                //DropEstado.ReadOnly = true;
                //DropMunicipio.ReadOnly = true;
                //DropColonia.ReadOnly = true;

                //LblAlert.Text = "Correo Vacio";
            }
            Cargar_GridDatos();
        }

        protected void GvEmpleados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
            {
                //  Cuando el Mouse este dentro de el row coloca el estilo
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;" +
                                                                            "this.style.backgroundColor='#ccc'");

                // Cuando el Mouse salga de el row vuelve al estilo normal
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }

        protected void BtnEditar_Click(object sender, EventArgs e)
        {

            if (TbxNombre.Text.Trim() != string.Empty && TbxCorreo.Text.Trim() != string.Empty && TbxCalle.Text.Trim() != string.Empty && TbxNumero.Text.Trim() != string.Empty && DropRol.SelectedIndex != 0 && DropEstado.SelectedIndex != 0 && DropMunicipio.SelectedIndex != 0 && DropColonia.SelectedIndex != 0)
            {
                emp.correo = TbxCorreo.Text;
                emp.nombre = TbxNombre.Text;
                emp.rol_id = DropRol.SelectedIndex;
                string calle = TbxCalle.Text;
                int numero = int.Parse(TbxNumero.Text);
                int colonia = 0;
                read = con.Consultar("select colonias.id from estados join municipios " +
                                                    "on estados.id = municipios.estado_id  join colonias " +
                                                    "on colonias.clave = municipios.clave where estados.nombre like '%" + DropEstado.SelectedItem + "%' " +
                                                    "and municipios.nombre like '%" + DropMunicipio.SelectedItem + "%' " +
                                                    "and colonias.nombre like '%" + DropColonia.SelectedItem + "%'; ");

                if (read.HasRows)
                {
                    while (read.Read())
                    {
                        colonia = read.GetInt32(0);
                    }
                    read.NextResult();
                }
                
                LblAlert.Text =  con.Actualizar(emp.Actualizar(emp.GetId(TbxCorreo.Text),colonia,calle,numero));
                //editar usuario y guardar en la base de datos
                Cargar_GridDatos();
            }
            else
            {
                LblAlert.Text = "No Puede Editar un Registro con Campos Vacios";
            }
            Cargar_GridDatos();
        }
        private void Cargar_GridDatos()
        {
            MySqlCommand consulta = new MySqlCommand(string.Format("select roles.nombre as Rol,empleados.nombre as Nombre,empleados.correo as Correo," +
                                                                   "empleados.fecha_alta, empleados.fecha_actualizacion," +
                                                                   "estados.nombre as Estado, municipios.nombre as Municipio, colonias.nombre as Colonia," +
                                                                   "colonias.codigo_postal as Codigo_Postal, direcciones.calle as Calle, direcciones.numero as Numero " +
                                                                   "from direcciones join colonias " +
                                                                   "on direcciones.colonia_id = colonias.id join estados " +
                                                                   "on estados.id = colonias.estado_id join municipios " +
                                                                   "on municipios.estado_id = estados.id " +
                                                                   "and municipios.clave = colonias.clave join empleados " +
                                                                   "on empleados.id = direcciones.empleados_id join roles " +
                                                                   "on roles.id = empleados.rol_id; "), Conection.Conectar());
            MySqlDataAdapter adapter = new MySqlDataAdapter(consulta);
            DataTable dtable = new DataTable();
            int resultados = adapter.Fill(dtable);
            LblResultados.Text = resultados.ToString();
            GvEmpleados.DataSource = dtable;
            GvEmpleados.DataBind();
            Conection.Conectar().Close();

            // Limpiar_Controles();
        }

    }
}