﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/MainPage.Master" AutoEventWireup="true" CodeBehind="Gestion_.aspx.cs" Inherits="ControlEmpleados.Main.Gestion_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 32px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server"> 
        <h1 class="titulo">Registro de Usuarios</h1>
        <div>
            
            <asp:Label ID="LblAlert" runat="server" ForeColor="#33CC33" Text="" style="color:#33CC33;margin-left: 33.5%;font-size: 150%"></asp:Label>
        </div>
    <div class="container">
          <div class="row">
              <div class="col-md-12">
               <div class="card col-md-6">
                <h3 class="card-header primary-color white-text">Registrar</h3>
                <div class="card-body">
                            <table class="w-100">
                                <tr>
                                    <td>
                                          <div class="md-form dato1 col-md-12">
                                            <i class="fa fa-user prefix"></i>
                                            <asp:TextBox ID="TbxNombre" runat="server" class="form-control" placeholder="Nombre"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <div class="md-form dato1 col-md-12 correo">
                                            <i class="fa fa-envelope prefix"></i>
                                            <asp:TextBox ID="TbxCorreo" runat="server" class="form-control"  placeholder="Correo"></asp:TextBox>
                                        </div>
                   
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%-- <div class="btn-group col-md-3">
                                            <button class="btn btn-primary dropdown-toggle col-md-12 filtros" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Estado</button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#">Aguascalientes</a>
                                                <a class="dropdown-item" href="#">Baja California</a>
                                            </div>
                                        </div>--%>
                                        <div class="btn-group col-md-12 group-drops">
                                            <i class="fa fa-sitemap prefix fa-3x col-md-1"></i>
                                            <asp:DropDownList ID="DropRol" runat="server" AutoPostBack="True" data-toggle="dropdown" placeholder="Rol" class="btn btn-primary dropdown-toggle col-md-12 drop">    
                                                <asp:ListItem >-- Roles --</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style1">
                                        <div class="btn-group col-md-12 group-drops">
                                            <i class="fa fa-hand-o-right prefix fa-3x col-md-1"></i>
                                            <asp:DropDownList ID="DropEstado" runat="server" data-toggle="dropdown" placeholder="Rol" OnSelectedIndexChanged="DropEstado_SelectedIndexChanged" CausesValidation="True" AutoPostBack="True" class="btn btn-primary dropdown-toggle col-md-12 drop">
                                                <asp:ListItem>-- Estados --</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="btn-group col-md-12 group-drops">
                                        <i class="fa fa-hand-o-right prefix fa-3x col-md-1"></i>
                                        <asp:DropDownList ID="DropMunicipio" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropMunicipio_SelectedIndexChanged" class="btn btn-primary dropdown-toggle col-md-12 drop">
                                            <asp:ListItem>-- Municipios --</asp:ListItem>   
                                        </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style1">
                                        <div class="btn-group col-md-12 group-drops">   
                                        <i class="fa fa-hand-o-right prefix fa-3x col-md-1"></i>
                                        <asp:DropDownList ID="DropColonia" runat="server" AutoPostBack="True" class="btn btn-primary dropdown-toggle col-md-12 drop" >
                                            <asp:ListItem>-- Colonias --</asp:ListItem>   
                                        </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                         <div class="md-form dato1 col-md-12">
                                            <i class="fa fa-map-marker prefix"></i>
                                            <asp:TextBox ID="TbxCalle" runat="server"  class="form-control" placeholder="Calle"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="md-form dato1 col-md-12">
                                            <i class="fa fa-hashtag prefix"></i>
                                            <asp:TextBox ID="TbxNumero" runat="server" class="form-control"  placeholder="Número"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="BtnAgregar" runat="server" Text="Agregar" OnClick="BtnAgregar_Click" Class="btn btn-success col-md-6 Agregar" />
                   
                                    </td>
                                </tr>
                            </table>
                           </div>
                          </div>
                    </div>
              </div>
          </div>



        
  </form>
</asp:Content>
