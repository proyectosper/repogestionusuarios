﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/MainPage.Master" AutoEventWireup="true" CodeBehind="Gestion.aspx.cs" Inherits="ControlEmpleados.Main.Gestion" EnableEventValidation="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 22px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="edicion-titulo">Edición de Usuarios</h1>
    <asp:Label ID="LblAlert" runat="server" ForeColor="#33CC33" Text=""  style="font-size: 150%;margin-left: 33%;color:  green;"></asp:Label>
<div class="card card-actualizar">
    <div class="card-body card-actualizar">

        <form class="container-fluid" runat="server">
            <div class="row row-actualizar">
                <div class="md-form col">                 
                    <i class="fa fa-user prefix"></i>
                    <asp:TextBox ID="TbxNombre" runat="server" class="form-control" placeholder="Nombre"></asp:TextBox>
                </div>
                <div class="md-form col">
                    <i class="fa fa-envelope prefix"></i>
                    <asp:TextBox ID="TbxCorreo" runat="server" class="form-control"  placeholder="Correo"></asp:TextBox>
                 </div>
           </div>
           <div class="row row-actualizar">
                <div class="md-form col">                 
                    <i class="fa fa-sitemap prefix fa-3x col-md-1 icon-rol"></i>
                    <asp:DropDownList ID="DropRol" runat="server" AutoPostBack="True" data-toggle="dropdown" placeholder="Rol" class="btn btn-primary dropdown-toggle col-md-12 drop-actualizar">    
                        <asp:ListItem >-- Roles --</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="md-form col">                 
                    <i class="fa fa-hand-o-right prefix fa-3x col-md-1 icon-estado"></i>
                    <asp:DropDownList ID="DropEstado" runat="server" data-toggle="dropdown" placeholder="Rol" OnSelectedIndexChanged="DropEstado_SelectedIndexChanged" CausesValidation="True" AutoPostBack="True" class="btn btn-primary col-md-12 drop-actualizar">
                        <asp:ListItem>-- Estados --</asp:ListItem>
                    </asp:DropDownList>
                </div>
           </div>
            <div class="row row-actualizar">
                <div class="md-form col">                 
                    <i class="fa fa-hand-o-right prefix fa-3x col-md-1 icon-municipio"></i>
                    <asp:DropDownList ID="DropMunicipio" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropMunicipio_SelectedIndexChanged" class="btn btn-primary dropdown-toggle col-md-12 drop-actualizar ">
                        <asp:ListItem>-- Municipios --</asp:ListItem>   
                    </asp:DropDownList>
                </div>
                <div class="md-form col">                 
                    <i class="fa fa-hand-o-right prefix fa-3x col-md-1 icon-colonia"></i>
                    <asp:DropDownList ID="DropColonia" runat="server" AutoPostBack="True" class="btn btn-primary dropdown-toggle col-md-12 drop-actualizar" >
                        <asp:ListItem>-- Colonias --</asp:ListItem>   
                    </asp:DropDownList>
                </div>
           </div>
             <div class="row row-actualizar">
                <div class="md-form col">                 
                      <i class="fa fa-map-marker prefix"></i>
                      <asp:TextBox ID="TbxCalle" runat="server"  class="form-control" placeholder="Calle"></asp:TextBox>
                </div>
                <div class="md-form col">
                        <i class="fa fa-hashtag prefix"></i>
                        <asp:TextBox ID="TbxNumero" runat="server" class="form-control"  placeholder="Número"></asp:TextBox>                                       
                 </div>
           </div>
            <div class="row">
                <div class="md-form col">                 
                       <asp:Button ID="BtnEditar" runat="server" Text="Editar" OnClick="BtnEditar_Click" Class="btn btn-success col-md-6 Agregar" />
                </div>
            </div>
     </div>
  </div>

         <h2 class="card-title gestion-titulo">Resultados (<asp:Label ID="LblResultados" Text="0" runat="server"/>) </h2>
         <div class="container cont-grid">
            <div class="row">
                <asp:GridView ID="GvEmpleados" runat="server"  CssClass="table table-bordered bs-table" class="col-md-10" OnRowDataBound="GvEmpleados_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderStyle-ForeColor="Black" HeaderText="Editar">
                            <ItemTemplate>
                                 <asp:Button ID="BtnActualizar" runat="server" style = "display:block" Text="Editar" CommandArgument="BtnActualizar"  OnClick="BtnActualizar_Click" class="btn btn-cyan" />
<%--                                <asp:Button ID="BtnEditar" runat="server" Text="Editar" class="btn btn-cyan" OnClick="BtnEditar_Click"/>--%>
                            </ItemTemplate>

                        </asp:TemplateField>  
                       
                       <%-- <asp:CommandField SelectText="Editar" ShowSelectButton="True" ButtonType="Button" HeaderText="Editar" >
                       
                        <ControlStyle CssClass="btn-edit" />
                        </asp:CommandField>
                        <asp:CommandField ButtonType="Button"  DeleteText="Eliminar"  EditText="Eliminar" HeaderText="Eliminar" SelectText="Eliminar" ShowDeleteButton="True" />
                  --%>  

                    </Columns>
                </asp:GridView>
            </div>
        </div>
            
       </form>
</asp:Content>
