﻿<%@ Page  Title="" Language="C#" MasterPageFile="~/Main/MainPage.Master" AutoEventWireup="true" CodeBehind="Busqueda.aspx.cs" Inherits="ControlEmpleados.Main.Busqueda" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager2" runat="server">

                </asp:ScriptManager>
<div class="contenedor">    
    <h1 class="card-title gestion-titulo">Gestión de Usuarios</h1>
    
    <div class="card card-filter">
        <div class="card-body row">
            
            <h4 class="card-title col-md-12">Filtros de Busqueda</h4>

                    <div class="md-form dato2 col-md-3">
                        <i class="fa fa-user prefix"></i>
                        <asp:TextBox ID="TbxNombre" runat="server" class="form-control" placeholder="Nombre"></asp:TextBox>
                    </div>
                   
                    <div class="btn-group col-md-3">
                        <div class="row col-md-12 row-filtrar">
                        </div>
                        <div class="row col-md-12 row-filtrar-drop">
                            <asp:DropDownList ID="DropEstados" runat="server" AutoPostBack="True" class="btn btn-primary dropdown-toggle col-md-12 filtros" data-toggle="dropdown" placeholder="Rol" OnSelectedIndexChanged="DropEstados_SelectedIndexChanged">    
                                <asp:ListItem >-- Estados --</asp:ListItem>
                            </asp:DropDownList>     
                        </div>
                    </div>
                    
                    <div class="btn-group col-md-3">
                        <div class="row col-md-12 row-filtrar">
                        </div>
                        <div class="row col-md-12 row-filtrar-drop">
                            <asp:DropDownList ID="DropMunicipios" runat="server" AutoPostBack="True" class="btn btn-primary dropdown-toggle col-md-12 filtros" data-toggle="dropdown" placeholder="Rol" OnSelectedIndexChanged="DropMunicipios_SelectedIndexChanged">    
                                <asp:ListItem >-- Municipios --</asp:ListItem>
                            </asp:DropDownList>     
                        </div>
                    </div>

                    
                    <div class="btn-group col-md-3">
                        <div class="row col-md-12 row-filtrar">
                        </div>
                        <div class="row col-md-12 row-filtrar-drop">
                            <asp:DropDownList ID="DropColonias" runat="server" AutoPostBack="True" class="btn btn-primary dropdown-toggle col-md-12 filtros" data-toggle="dropdown" placeholder="Rol">    
                                <asp:ListItem >-- Colonias --</asp:ListItem>
                            </asp:DropDownList>     
                        </div>
                    </div>
                        <asp:Button ID="BtnQuitar" runat="server" Text="x"  Class="btn btn-danger col-md-1 filtrar fil-margin" OnClick="BtnQuitar_Click"/>
                        <asp:Button ID="BtnFiltrar" runat="server" Text="Filtrar"  Class="btn btn-success col-md-2 filtrar" OnClick="BtnFiltrar_Click" />
             </div>
        </div>
    </div>
<!--Card-Busqueda-->
        
    <asp:Label ID="Lblrespuesta" runat="server" Text="" style="font-size: 150%;margin-left: 37%;color:  green;"></asp:Label>
    <h2 class="card-title gestion-titulo titulo-gest">Resultados (<asp:Label ID="LblResultados" Text="0" runat="server"/>) </h2>

        <div class="container cont-grid">
            <div class="row">
                <asp:GridView ID="GvEmpleados" runat="server"  CssClass="table table-bordered bs-table" class="col-md-10" OnRowDataBound="GvEmpleados_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderStyle-ForeColor="Black" HeaderText="Editar">
                            <ItemTemplate>
                                 <asp:Button ID="BtnEditar" runat="server" style = "display:block" Text="Editar" CommandArgument="BtnEditar"  OnClick="BtnEditar_Click" class="btn btn-cyan" />
<%--                                <asp:Button ID="BtnEditar" runat="server" Text="Editar" class="btn btn-cyan" OnClick="BtnEditar_Click"/>--%>
                            </ItemTemplate>

                        </asp:TemplateField>  
                        <asp:TemplateField HeaderStyle-ForeColor="Black" HeaderText="Eliminar">
                            <ItemTemplate>
                               <asp:Button ID="BtnEliminar" runat="server"  style = "display:block" Text="Eliminar" CommandArgument="BtnEliminar" OnClick="BtnEliminar_Click" class="btn btn-pink" />
<%--                                <asp:Button ID="BtnEditar" runat="server" Text="Editar" class="btn btn-cyan" OnClick="BtnEditar_Click"/>--%>
                          </ItemTemplate>
                         </asp:TemplateField>  
                       <%-- <asp:CommandField SelectText="Editar" ShowSelectButton="True" ButtonType="Button" HeaderText="Editar" >
                       
                        <ControlStyle CssClass="btn-edit" />
                        </asp:CommandField>
                        <asp:CommandField ButtonType="Button"  DeleteText="Eliminar"  EditText="Eliminar" HeaderText="Eliminar" SelectText="Eliminar" ShowDeleteButton="True" />
                  --%>  

                    </Columns>
                </asp:GridView>
            </div>
        </div>
                
               
                    <asp:Label ID="Label1" runat="server" Text="Inserte su Nombre" Visible="false"></asp:Label>
                    <asp:TextBox ID="TextBox1" runat="server" Visible="false"></asp:TextBox>



      
    </form>


</asp:Content>
