﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using System.Web.UI.WebControls;

namespace ControlEmpleados
{
    public static class Base
    {
        static Conection con = new Conection();

        static MySqlDataReader read;

        public static string dato_unico = string.Empty;
        ///Metodo Para Cargar el Drop Estático
        public static void Cargar_DropEstatico(string sentencia, DropDownList drop, string itemZero)
        {
            read = con.Consultar(sentencia);

            drop.DataSource = read;
            drop.DataTextField = "nombre";
            drop.DataValueField = "id";
            drop.DataBind();

            drop.Items.Insert(0, itemZero);
        }
        ///Metodo Para Cargar el Drop Dinámico
        public static void Cargar_DropDinamico(string sentencia, DropDownList drop, string itemZero)
        {
            read = con.Consultar(sentencia);

            drop.DataSource = read;
            drop.DataTextField = "nombre";
            drop.DataValueField = "id";
            drop.DataBind();
            
            drop.Items.Insert(0, itemZero);

            if (read.HasRows)
            {
                while (read.Read())
                {
                    drop.DataValueField = read.GetInt32(0).ToString();
                    drop.DataTextField = read.GetString(1);
                }
                read.NextResult();
            }
            else
            {
                // LblAlert.Text = "Error en la consulta";
            }
            read.Close();
        }
    }

}